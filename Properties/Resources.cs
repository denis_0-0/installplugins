﻿// Decompiled with JetBrains decompiler
// Type: WpfApp1.Properties.Resources
// Assembly: WpfApp1, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7D930CC5-A7D1-492A-B44E-FF60E2246D5A
// Assembly location: D:\EP\Семейства ИО\00-ПРОГРАММИРОВАНИЕ\NewInstallPlugins.exe

using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;

namespace InstalPlugins.Properties
{
  [GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
  [DebuggerNonUserCode]
  [CompilerGenerated]
  internal class Resources
  {
    private static ResourceManager resourceMan;
    private static CultureInfo resourceCulture;

    internal Resources()
    {
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static ResourceManager ResourceManager
    {
      get
      {
        if (InstalPlugins.Properties.Resources.resourceMan == null)
          InstalPlugins.Properties.Resources.resourceMan = new ResourceManager("WpfApp1.Properties.Resources", typeof (InstalPlugins.Properties.Resources).Assembly);
        return InstalPlugins.Properties.Resources.resourceMan;
      }
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static CultureInfo Culture
    {
      get
      {
        return InstalPlugins.Properties.Resources.resourceCulture;
      }
      set
      {
        InstalPlugins.Properties.Resources.resourceCulture = value;
      }
    }
  }
}
