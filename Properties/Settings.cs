﻿// Decompiled with JetBrains decompiler
// Type: WpfApp1.Properties.Settings
// Assembly: WpfApp1, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7D930CC5-A7D1-492A-B44E-FF60E2246D5A
// Assembly location: D:\EP\Семейства ИО\00-ПРОГРАММИРОВАНИЕ\NewInstallPlugins.exe

using System.CodeDom.Compiler;
using System.Configuration;
using System.Runtime.CompilerServices;

namespace InstalPlugins.Properties
{
  [CompilerGenerated]
  [GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "11.0.0.0")]
  internal sealed class Settings : ApplicationSettingsBase
  {
    private static Settings defaultInstance = (Settings) SettingsBase.Synchronized((SettingsBase) new Settings());

    public static Settings Default
    {
      get
      {
        Settings defaultInstance = Settings.defaultInstance;
        return defaultInstance;
      }
    }
  }
}
