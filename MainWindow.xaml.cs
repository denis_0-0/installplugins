﻿// Decompiled with JetBrains decompiler
// Type: WpfApp1.MainWindow
// Assembly: WpfApp1, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7D930CC5-A7D1-492A-B44E-FF60E2246D5A
// Assembly location: D:\EP\Семейства ИО\00-ПРОГРАММИРОВАНИЕ\NewInstallPlugins.exe

using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Markup;

namespace InstalPlugins
{
  public partial class MainWindow : Window, IComponentConnector
  {
    public string path = @"C:\ProgramData\Autodesk\Revit\Addins\2019";
    public string path21 = @"C:\ProgramData\Autodesk\Revit\Addins\2021";
    public MainWindow()
    {
      if (Process.GetProcessesByName("Revit").Length > 0)
      {
        MessageBox.Show("Revit запущен. Необходимо закрыть Revit");
        this.Close();
      }

      this.InitializeComponent();
    }

    private void InstallSMPPlugs(object sender, RoutedEventArgs e)
    {
      for (int index = 0; index < 700; ++index)
      {
        try
        {

          //if (!Directory.Exists(path))
          //  Directory.CreateDirectory(path);
          foreach (string file in Directory.GetFiles(@"\\psmail\BIM_Model\00_Библиотека\04-Плагины\SMP28\2019", "*.*", SearchOption.AllDirectories))
          {
            File.Copy(file, file.Replace(@"\\psmail\BIM_Model\00_Библиотека\04-Плагины\SMP28\2019\SMP28Plugins", path), true);
          }
          foreach (string file in Directory.GetFiles(@"\\psmail\BIM_Model\00_Библиотека\04-Плагины\SMP28\2021", "*.*", SearchOption.AllDirectories))
          {
            File.Copy(file, file.Replace(@"\\psmail\BIM_Model\00_Библиотека\04-Плагины\SMP28\2021", path21), true);
            int num = (int)MessageBox.Show("Успешно");
          }

          break;
        }
        catch
        {
        }
        if (index == 699)
        {
          int num1 = (int)MessageBox.Show("Отмена установки", "Ошибка");
        }
      }
    }
    private void UpdateSMPPlugs(object sender, RoutedEventArgs e)
    {
      for (int index = 0; index < 700; ++index)
      {
        try
        {
          foreach (string file in Directory.GetFiles(@"\\psmail\BIM_Model\00_Библиотека\04-Плагины\SMP28\SMP28Plugins", "*.*", SearchOption.AllDirectories))
          {
            //File.Copy(file, file.Replace(@"\\psmail\BIM_Model\00_Библиотека\04-Плагины\SMP28\SMP28Plugins", path), true);            
          }

          int num = (int)MessageBox.Show("Успешно");
          break;
        }
        catch
        {
        }
        if (index == 699)
        {
          int num1 = (int)MessageBox.Show("Отмена установки", "Ошибка");
        }
      }
    }
    private void InstallCheckLinks(object sender, RoutedEventArgs e)
    {
      for (int index = 0; index < 700; ++index)
      {
        try
        {
          foreach (string file in Directory.GetFiles(@"\\psmail\BIM_Model\00_Библиотека\04-Плагины\Checklinks", "*.*", SearchOption.AllDirectories))
            File.Copy(file, file.Replace(@"\\psmail\BIM_Model\00_Библиотека\04-Плагины\Checklinks", path), true);
          int num = (int)MessageBox.Show("Успешно");
          break;
        }
        catch
        {
        }
        if (index == 699)
        {
          int num1 = (int)MessageBox.Show("Отмена установки", "Ошибка");
        }
      }
    }

    private void UpdateCheckLinks(object sender, RoutedEventArgs e)
    {
      for (int index = 0; index < 700; ++index)
      {
        try
        {

          int num = (int)MessageBox.Show("Успешно");
          break;
        }
        catch
        {
        }
        if (index == 699)
        {
          int num1 = (int)MessageBox.Show("Отмена установки", "Ошибка");
        }
      }
    }

    private void InstallLookup(object sender, RoutedEventArgs e)
    {
      for (int index = 0; index < 700; ++index)
      {
        try
        {
          foreach (string file in Directory.GetFiles(@"\\psmail\BIM_Model\00_Библиотека\04-Плагины\LookUp", "*.*", SearchOption.AllDirectories))
            File.Copy(file, file.Replace(@"\\psmail\BIM_Model\00_Библиотека\04-Плагины\LookUp", path), true);
          int num = (int)MessageBox.Show("Успешно");
          break;
        }
        catch
        {
        }
        if (index == 699)
        {
          int num1 = (int)MessageBox.Show("Отмена установки", "Ошибка");
        }
      }
    }

    


  }
}
