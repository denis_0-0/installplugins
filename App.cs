﻿// Decompiled with JetBrains decompiler
// Type: WpfApp1.App
// Assembly: WpfApp1, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7D930CC5-A7D1-492A-B44E-FF60E2246D5A
// Assembly location: D:\EP\Семейства ИО\00-ПРОГРАММИРОВАНИЕ\NewInstallPlugins.exe

using System;
using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Windows;

namespace InstalPlugins
{
  public class App : Application
  {
    [DebuggerNonUserCode]
    [GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    public void InitializeComponent()
    {
      this.StartupUri = new Uri("MainWindow.xaml", UriKind.Relative);
    }

    [STAThread]
    [DebuggerNonUserCode]
    [GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    public static void Main()
    {
      App app = new App();
      app.InitializeComponent();
      app.Run();
    }
  }
}
